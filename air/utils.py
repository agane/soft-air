"""
Utility functions for dealing with soft prediction.
"""
import torch

def concat_steps(z_pres):
    ''' Compute the concatenated predictions, where we add an initial (unused)
    step with constant estimated probability 1.

    Args:
        z_pres: list of size args.model_steps (default=3), where each element
            is of size (batch_size, 1). Note that initially, z_halt elements
            are close to 0.5, so the z_pres elements are [~0.5, ~0.1, 0].

    Outs:
        z_pres_cat: Variable of size (batch_size, n_steps + 1).
    '''
    # FIXME: do grads backprop correctly here?
    z_pres_init = z_pres[0].data.clone().fill_(1)
    if z_pres[0].is_cuda:
        z_pres_init = z_pres_init.cuda()
    z_pres_cat = torch.cat([z_pres_init] + z_pres, 1)
    return z_pres_cat

def extract_inferred_counts(z_pres_cat):
    ''' Extract a tensor containing the inferred counts per batch element,
    by searching for the first zero.

    Args:
        z_pres_cat: Variable of size (batch_size, n_steps + 1).
    Return:
        inferred_counts: Tensor
    '''
    # Compute the argmin per element and check whether it's equal to 0. The min
    # argmin have sizes (batch_size, ). I could not find documentation
    # confirming that the returned argmax is the index of the first max value,
    # tests indicate that this is indeed the case.
    z_pres_min, z_pres_argmin = torch.min(z_pres_cat.data, 1)
    # We extract the index z_pres_N of the first element whose existence
    # probability is zero, z_pres_N has size (batch_size, )
    z_pres_N = torch.where(z_pres_min == 0,
                           z_pres_argmin,
                           z_pres_argmin.clone().fill_(z_pres_cat.size(1)))
    inferred_counts = (z_pres_N - 1)
    return inferred_counts
